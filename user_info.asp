<!--#include file="json.asp"-->
<!--#include file="conn.asp"-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%
'================================================================
'=   文件名称：user_info.asp                                    =
'=   实现功能：根据access_tocken openid获取userinfo             =
'=   QQ and Wx：676750993                                       =
'================================================================ 
	openid = request("openid")
	access_token = request("access_token")
	ip = request("ip")
  userinfoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token="&access_token
  userinfoUrl = userinfoUrl & "&openid="& openid &"&lang=zh_CN"
  
  set objXmlHttp=Server.CreateObject("MSXML2.XMLHTTP")   
  
  objXmlHttp.open "GET",userinfoUrl,false
  objXmlHttp.send()
  
  resultText=objXmlHttp.responseText
   
  set objXmlHttp=nothing
  
  '解析json
  '{   
  '"openid": "OPENID",
  '"nickname": NICKNAME,
  '"sex": 1,
  '"province":"PROVINCE",
  '"city":"CITY",
  '"country":"COUNTRY",
  '"headimgurl":"https://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46",
  '"privilege":[ "PRIVILEGE1" "PRIVILEGE2"     ],
  '"unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
  '} 
  Set res = parseJSON(resultText)
  'response.write userinfoUrl
  nickname = res.nickname
  headimg = res.headimgurl
  
  currTime=now()
 
 '更新数据库
  set rs=server.CreateObject("adodb.recordset")
	sql="update userinfo set nickname='"& nickname &"',headimg='"& headimg &"' where ip='"& ip &"'"
	on error resume next
	rs.open sql,conn
	set rs=nothing
  
  session("openid") = openid
  'session("nickname") = nickname
  session("headimg") = headimg
  session("ip")=ip
  
  Response.Redirect "show.asp?nickname="& nickname
  
%>