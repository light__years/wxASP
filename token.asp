<%'================================================================
'=   文件名称：tocken.asp                                       =
'=   实现功能：获取tocken                                         =
'=   QQ and Wx：676750993                                       =
'================================================================ 
%>
<!--#include file="conn.asp"-->
<!--#include file="json.asp"-->
<!--#include file="config.asp"-->
<%
testToken = getToken()
response.write testToken

Function getToken

'先从数据库中获取未过期token
currTime=now()
'response.write currTime

set rs=server.CreateObject("adodb.recordset")
sql="select * from token  where expireTime >'" & currTime & "'"
rs.open sql,conn
if rs.eof then
	getTocken = rs("token")
	rs.Close 
	set rs=nothing
	Exit Function
end if
rs.Close 
set rs=nothing

'请求微信url接口获取tocken
tocken_url="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="&AppID&"&secret="&AppSecret

set objXmlHttp=Server.CreateObject("MSXML2.XMLHTTP")

'set objXmlHttp=Server.CreateObject("MSXML2.ServerXMLHTTP")
'objXmlHttp.setOption 2,SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS
'objXmlHttp.setRequestHeader "User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"

objXmlHttp.open "GET",tocken_url,false
objXmlHttp.send()

resultText=objXmlHttp.responseText
'response.write resultText
set objXmlHttp=nothing

'解析json access_tocken tocken值,expire_in 有效期 单位秒 7200
Set res = parseJSON(resultText)
token = res.access_token
expireSec = res.expires_in

'expireSec = 7200
'token = "test"


expireTime = DateAdd("s",expireSec,now())

'response.write "</br>"
'response.write expireTime


set rs=server.CreateObject("adodb.recordset")
sql = "update token set expireTime='"& expireTime &"',token='"& token &"'"

'response.write "</br>"

on error resume next
rs.open sql,conn
rs.Close
set rs=nothing

getToken=token

End Function

%>