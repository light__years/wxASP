# wxASP

#### 介绍
公众号网页授权，获取openid,access_tocken
用户扫码登录网站，记录用户昵称和openid

#### 软件架构
软件架构说明

1.  采用ASP最原生代码，访问微信服务号接口，进行授权，获取用户信息
2.  index.asp 首页 生成二维码，供用户微信扫码，采用用户授权方式:snsapi_userinfo，二维码中有回调地址
3.  access_token.asp 回调地址页面，接收code,通过code 获取openid 和 access_token
4.  auto_fresh.asp 定时是否扫描成功,获取openid
5.  user_info.asp 获取用户信息页面，通过openid和access_token 获取用户昵称和头像以及其他信息
6.  show.asp 用户信息展示页
7.  config.asp 配置信息页
8.  json.asp json解析
9.  conn.asp 数据库连接
10.  data/minidata.mdb  轻量级access数据库
11. js目录 jquery和二维码js库
12.  token.asp 普通token获取页
13.  minidata.mdb 表结构
```
    userinfo表
       id            AutoNumber --自增id
       openid        Text(50)   --openid，字符串类型
       nickname      Text(50)   --昵称，字符串类型
       headimg       Memo       --头像地址，字符串类型
       ceate_time    Text(50)   --插入时间，字符串类型
       ip            Text(18)   --网站地址，字符串类型
       access_token  Text(255)  --token，字符串类型
       login_state   Text(2)    --是否登录成功，字符串类型

    token表（普通token）
      id         AutoNumber --自增id
      tocken     Text(255)  --普通tocken,字符串类型
      expireTime Text(25)   --过期时间，字符串类型
```

#### 安装教程

1.  点击克隆下载zip包
2.  解压后放在网站根目录
3.  保证您的公众号类型是服务号


![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/101341_0e09e35c_1645697.png "屏幕截图.png")
#### 使用说明

1.  下载文件，将wxASP文件夹放入网站根目录
2.  配置config.asp:AppID为您的服务号id,AppSecret为您的服务号秘钥，domainUrl为您的网页域名
3.  访问 www.xxx.com/wxASP/index.asp

#### 演示

1.  测试地址 http://www.wood168.net/wxASP/index.asp

由于种种原因，白飘党太不要脸，顾删除了关键代码。若需要，请联系

### 联系
1.  qq:676750993
1.  wx:676750993
1.  邮箱:676750993@qq.com

### 捐赠

|  微信 |  支付宝 |
|---|---|
|  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/103418_66379f4d_1645697.png "屏幕截图.png") | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/103529_fd091a39_1645697.png "屏幕截图.png")|


